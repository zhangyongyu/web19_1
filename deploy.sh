# 手动
# git clone
# cd dir

# 拉代码
git pull

# 装依赖
apt-get install git python3 python3-pip supervisor nginx
pip3 install -U pip setuptools wheel
pip3 install jinja2 flask gunicorn

# 删掉 nginx default 设置
rm -f /etc/nginx/sites-enabled/default
rm -f /etc/nginx/sites-available/default

# 建立一个软连接
ln -s -f /home/ubuntu/web19_1/web20.conf /etc/supervisor/conf.d/web20.conf
# 不要再 sites-available 里面放任何东西
ln -s -f /home/ubuntu/web19_1/web20.nginx /etc/nginx/sites-enabled/web20

# 重启服务器
service supervisor restart
service nginx restart

echo 'deploy success'