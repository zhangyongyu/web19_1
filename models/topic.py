import time
from models import Model
from models.reply import Reply


class Topic(Model):
    @classmethod
    def get(cls, id):
        m = cls.find_by(id=id)
        m.views += 1
        m.save()
        return m

    def __init__(self, form):
        self.id = None
        self.views = 0
        self.title = form.get('title', '')
        self.content = form.get('content', '')
        # created time
        self.ct = int(time.time())
        # updated time
        self.ut = self.ct
        self.user_id = form.get('user_id', '')

    def user(self):
        from models.user import User
        u = User.find(self.user_id)
        return u

    def replies(self):
        ms = Reply.find_all(topic_id=self.id)
        return ms

    def reply_count(self):
        count = len(self.replies())
        return count
