from flask import (
    render_template,
    request,
    redirect,
    url_for,
    Blueprint,
)

from routes import *

from models.topic import Topic


main = Blueprint('topic', __name__)


@main.route("/")
def index():
    ms = Topic.all()
    u = current_user()
    return render_template("topic/index.html", ms=ms, u=u)


@main.route('/<int:id>')
def detail(id):
    # http://localhost:2000/topic/1
    m = Topic.get(id)

    # 不应该放在路由里面
    # m.views += 1
    # m.save()

    # 传递 topic 的所有 reply 到 页面中
    return render_template("topic/detail.html", topic=m)


@main.route("/add", methods=["POST"])
def add():
    form = request.form
    u = current_user()
    m = Topic.new(form, user_id=u.id)
    return redirect(url_for('.detail', id=m.id))


@main.route("/new")
def new():
    return render_template("topic/new.html")
